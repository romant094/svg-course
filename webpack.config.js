const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const isDev = process.env.NODE_ENV === 'dev'
const isProd = !isDev
const isAnalyze = process.env.ANALYZE === 'true'

const filename = (filename) => {
  const nameArr = filename.split('.')
  const ext = nameArr.pop()
  const name = nameArr.join('.')
  return isDev ? `${name}.${ext}` : `${name}.[hash:8].${ext}`
}

module.exports = {
  entry: path.resolve(__dirname, 'src', 'index.js'),
  output: {
    filename: filename('bundle.js'),
    path: path.resolve(__dirname, './dist'),
    clean: true
  },
  mode: isDev ? 'development' : 'production',
  devServer: {
    port: 9000,
    open: true,
    host: '0.0.0.0',
    static: path.resolve(__dirname, 'src')
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src', 'index.hbs'),
      filename: 'index.html',
      inject: 'body',
      minify: false
    }),
    new MiniCssExtractPlugin({
      filename: filename('styles.css')
    }),
    isAnalyze && new BundleAnalyzerPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [isProd ? MiniCssExtractPlugin.loader : 'style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader'
      }
    ]
  }
}
