import { expandCodeBlocks } from './js/highlights'
import { createNavbar } from './js/navbar'
import { setupHeader } from './js/header'

import './styles/main.scss'

document.addEventListener('DOMContentLoaded', () => {
  createNavbar()
  expandCodeBlocks()
  setupHeader()
})
