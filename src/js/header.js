const header = document.querySelector('header')

const toggleHeaderClass = () => {
  if (window.scrollY > 52) {
    header.classList.add('scroll')
  } else {
    header.classList.remove('scroll')
  }
}

export const setupHeader = () => {
  const { height } = header.getBoundingClientRect()
  document.querySelector(':root').style.setProperty('--headerHeight', `${height + 20}px`)

  toggleHeaderClass()

  window.addEventListener('scroll', () => {
    toggleHeaderClass()
  })
}
