const createLink = (id, text) => {
  const link = document.createElement('a')
  link.href = `#${id}`
  link.textContent = text
  return link
}

export const createNavbar = () => {
  const nav = document.querySelector('nav')
  const headings = document.querySelectorAll('h3')

  headings.forEach((heading) => {
    const article = heading.closest('article')
    const title = heading.textContent.replace(/source/, '').trim()
    const id = title.toLowerCase().replaceAll(' ', '-')

    article.id = id
    const link = createLink(id, title)
    nav.appendChild(link)
  })
}
