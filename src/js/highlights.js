import hlCode from 'highlight.js/lib/core'
import xml from 'highlight.js/lib/languages/xml'
hlCode.registerLanguage('xml', xml)

export const expandCodeBlocks = () => {
  document.querySelectorAll('article').forEach((article) => {
    const svgContainer = article.querySelector('.flex')
    const svgs = Array.from(svgContainer.childNodes).filter((node) => node.nodeName === 'svg')
    const html = svgContainer.innerHTML.replace(/ {8,10}/g, '').replace('\n', '')
    const pre = document.createElement('pre')
    const classes = ['theme-vs2015', 'code']
    classes.forEach((clz) => pre.classList.add(clz))
    pre.innerHTML = hlCode.highlight(html, { language: 'xml' }).value

    const containerAppend = svgs.length > 1 ? article : svgContainer
    containerAppend.appendChild(pre)
  })
}
