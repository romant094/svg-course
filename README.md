# SVG challenge

## Inspired by
- [SVG Tutorial](https://svg-tutorial.com/)

## Deployment links
- [Gitlab Pages](https://svg-course-romant094-9d10bb7f6537a573eec914983bebe9579266d8f4aa.gitlab.io)
- [Vercel](https://svg-course-plum.vercel.app/)

## TODO
✅️ code of the svg on the right of the images  
⛔️ add opportunity of copying code (is it really needed without css?)  
✅️ header styles on scroll  
✅️ font and styling  
✅️ build  
✅️ deploy to Gitlab Pages  
✅ usage of handlebars and code decomposition  
✅️ make content centered  
✅️ make content responsive  
✅️ remove extra spaces in the code blocks  
⛔️ replace all partials with template (if possible) - use svgs, headlines and urls as variables  
✅️ encapsulate logic from index.js  
⛔️ add gradient overlays on the sides of the navbar (from transparent to #fff) depending on scroll position: only right, both, only left  
✅ create production build  
✅ count header and set its height to count padding and margin for sections  
✅ remove Contents from header   
⛔️ add app version from gitlab ci  
